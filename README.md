# Twirp-REST-Gen

A code generation tool which allows you to host REST services on top of Twirp. The code generated seemlesly combines the two and allows you to support both service-to-service and client-to-service communication easily.

## Installation

`go get bitbucket.org/noxdew/twirp-rest-gen`

## Usage

```go
// Some imports

// Put this after your generate for twirp
// For the example config see below
//go:generate twirp-rest-gen -s service.yml

func main() {
    // Create your Twirp server
    helloWorldServer := proto.NewHelloWorldServer(service, hook)
    twirpRestServer := proto.NewFooBarServer(service, hook)

    // Combine using the generated package by twirp-rest-gen
    // the router is optional in case you want to add routes manually,
    // feel free to pass word nil
    twirpRestServer := proto.NewTwirpRestServer(helloWorldServer, fooBarServer, router)

    // Start your server. Use any of the many different ways to start it up
    log.Fatal(http.ListenAndServe(":8080", twirpRestServer))
}
```

## Configs

```yaml
# The golang package
package: gopackage
# (optional) The global service name. If you have only a single
# service use this, it will be the default for each route.
# At least `service` or `serviceName` must be defined
service: HelloWorldService
# (optional) This will be used as the public name. If you have more
# than one proto services you can use this to give it a third name
serviceName: PublicServiceName
# List of all routes
routes:
    # The HTTP methods supported, can be a list
  - method:
      - GET
      - POST
    # The path to this function, check github.com/gorilla/mux
    # for details on path parameters
    path: /hello/{name}

    # (optional) Defines the service this function is part of.
    # Either top level or route level `service` must exist.
    # service: HelloWorldService

    # The function in the service to be called to handle requests
    function: SayHello
  - method: GET
    path: /foo
    service: FooServiceName
    function: MakeFoo
```

## Twirp

Twirp is RPC framework from Twitch. Check it out here: https://github.com/twitchtv/twirp

## Contributing

Feel free to create issues or contribute code. We are reviewing them regularly.
