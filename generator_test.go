package main

import (
	"testing"

	assert "github.com/stretchr/testify/assert"
	suite "github.com/stretchr/testify/suite"
)

type GeneratorTestSuite struct {
	suite.Suite
}

func (suite *GeneratorTestSuite) TestToProsSingle() {
	res := toProps([]string{"GET"})
	assert.Equal(suite.T(), `"GET"`, res)
}

func (suite *GeneratorTestSuite) TestToProsMulti() {
	res := toProps([]string{"GET", "POST"})
	assert.Equal(suite.T(), `"GET", "POST"`, res)
}

func (suite *GeneratorTestSuite) TestContainsTrue() {
	res := contains([]string{"GET", "POST"}, "GET")
	assert.True(suite.T(), res)

	res = contains([]string{"GET", "POST"}, "POST")
	assert.True(suite.T(), res)
}

func (suite *GeneratorTestSuite) TestContainsFalse() {
	res := contains([]string{"GET", "POST"}, "PUT")
	assert.False(suite.T(), res)

	res = contains([]string{"GET", "POST"}, "get")
	assert.False(suite.T(), res)
}

func TestGeneratorSuit(t *testing.T) {
	suite.Run(t, new(GeneratorTestSuite))
}
