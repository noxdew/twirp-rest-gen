package main

import (
	"flag"
)

func main() {
	source := flag.String("s", "", "the path to the source yml file")
	destination := flag.String("o", ".", "the path where the generated files should be written out")
	flag.Parse()

	if *source == "" {
		flag.PrintDefaults()
		return
	}

	conf := Parse(*source)
	conf.CreateGenerator(*destination).Generate()
}
