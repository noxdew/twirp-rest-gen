package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"

	"go.uber.org/zap"
	yaml "gopkg.in/yaml.v2"
)

// Config defines the top level config for the generator
type Config struct {
	FileName    string   `yaml:"-" json:"-"`
	Package     string   `yaml:"package" json:"package"`
	Service     string   `yaml:"service" json:"service"`
	ServiceName string   `yaml:"serviceName" json:"serviceName"`
	Routes      []*Route `yaml:"routes" json:"routes"`
}

// Route defines the per route config for the generator
type Route struct {
	Method    *string     `yaml:"-" json:"-"`
	Methods   *[]string   `yaml:"-" json:"-"`
	MethodRaw interface{} `yaml:"method" json:"method"`
	Service   string      `yaml:"service" json:"service"`
	Path      string      `yaml:"path" json:"path"`
	Function  string      `yaml:"function" json:"function"`
}

// In order to support yaml and json we need to create two parsers
type parser interface {
	Unmarshal([]byte, interface{}) error
}

type yamlParser struct{}

func (yamlParser) Unmarshal(data []byte, dest interface{}) error {
	return yaml.Unmarshal(data, dest)
}

type jsonParser struct{}

func (jsonParser) Unmarshal(data []byte, dest interface{}) error {
	return json.Unmarshal(data, dest)
}

// The access to the OS needs to be abstracted for testability
type accessor interface {
	ReadFile(path string) ([]byte, error)
	Exists(path string) bool
}

type fsAccessor struct{}

func (fsAccessor) Exists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func (fsAccessor) ReadFile(path string) ([]byte, error) {
	return ioutil.ReadFile(path)
}

// Parse parses a config file from the given file and returns the result.
// Also resolves the generic fields into the specific ones (Method)
func Parse(path string) Config {
	if strings.HasSuffix(path, ".yml") {
		return parse(path, fsAccessor{}, yamlParser{})
	}
	return parse(path, fsAccessor{}, jsonParser{})
}

func parse(filePath string, ac accessor, p parser) Config {
	confData, err := ac.ReadFile(filePath)
	if err != nil {
		zap.L().Panic("Failed to load config",
			zap.String("path", filePath),
			zap.Error(err),
		)
	}

	conf := Config{}
	err = p.Unmarshal(confData, &conf)
	if err != nil {
		zap.L().Panic("Failed to parse config",
			zap.String("path", filePath),
			zap.Error(err),
		)
	}

	// resolve the methods into the typed variables
	for _, route := range conf.Routes {
		arr, ok := route.MethodRaw.([]interface{})
		if ok {
			methods := []string{}
			for _, method := range arr {
				str, ok := method.(string)
				if ok {
					methods = append(methods, str)
				}
			}
			route.Methods = &methods
			continue
		}

		str, ok := route.MethodRaw.(string)
		if ok {
			route.Method = &str
		}
	}

	// We need the name to generate the resulting file name
	conf.FileName = filepath.Base(filePath)
	conf.FileName = strings.TrimSuffix(conf.FileName, path.Ext(conf.FileName))

	return conf
}
