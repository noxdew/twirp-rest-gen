package main

import (
	"testing"

	assert "github.com/stretchr/testify/assert"
	suite "github.com/stretchr/testify/suite"
)

//go:generate mockery -name=accessor -inpkg

type ConfigTestSuite struct {
	suite.Suite
	ma *mockAccessor
}

func (suite *ConfigTestSuite) SetupTest() {
	suite.ma = new(mockAccessor)
}

func (suite *ConfigTestSuite) TestParseParsesConfigYaml() {
	yaml := `
package: testpackage
service: ServiceName
serviceName: FancyServiceName
routes:
  - method:
      - GET
      - POST
    path: /hello/{name}
    function: SayHello
  - method: GET
    path: /foo
    service: FooServiceName
    function: MakeFoo
`
	path := "config/service.yml"
	suite.ma.On("ReadFile", path).Return([]byte(yaml), nil)

	conf := parse(path, suite.ma, yamlParser{})

	suite.ma.AssertExpectations(suite.T())
	assert.Equal(suite.T(), "service", conf.FileName, "filename should be parsed properly from the path")
	assert.Equal(suite.T(), "FancyServiceName", conf.ServiceName, "Incorrect population of the config from the yaml - route.ServiceName")
	assert.Equal(suite.T(), "ServiceName", conf.Service, "Incorrect population of the config from the yaml - route.Service")
	assert.Equal(suite.T(), "testpackage", conf.Package, "Incorrect population of the config from the yaml - route.Package")
	assert.Equal(suite.T(), 2, len(conf.Routes), "2 routes must be parsed")
	assert.True(suite.T(), assert.ObjectsAreEqualValues([]string{"GET", "POST"}, conf.Routes[0].getMethods()), "Multiple methods must be parsed and populated correctly")
	assert.Nil(suite.T(), conf.Routes[0].Method, "Multiple methods must be parsed and populated correctly (single missing)")
	assert.Equal(suite.T(), "GET", *conf.Routes[1].Method, "Single method must be parsed and populated correctly")
	assert.Nil(suite.T(), conf.Routes[1].Methods, "Single method must be parsed and populated correctly (multi missing)")
	assert.Equal(suite.T(), "/hello/{name}", conf.Routes[0].Path, "Incorrect population of the config from the yaml - route[0].path")
	assert.Equal(suite.T(), "/foo", conf.Routes[1].Path, "Incorrect population of the config from the yaml - route[1].path")
	assert.Equal(suite.T(), "SayHello", conf.Routes[0].Function, "Incorrect population of the config from the yaml - route[0].function")
	assert.Equal(suite.T(), "MakeFoo", conf.Routes[1].Function, "Incorrect population of the config from the yaml - route[1].function")
	assert.Equal(suite.T(), "FooServiceName", conf.Routes[1].Service, "Incorrect population of the config from the yaml - route[1].service")
}

func (suite *ConfigTestSuite) TestParseParsesConfigJson() {
	json := `{
	"package": "testpackage",
	"service": "ServiceName",
	"serviceName": "FancyServiceName",
	"routes": [{
		"method": ["GET", "POST"],
		"path": "/hello/{name}",
		"function": "SayHello"
	}, {
		"method": "GET",
		"path": "/foo",
		"service": "FooServiceName",
		"function": "MakeFoo"
	}]
}
`
	path := "config/service.yml"
	suite.ma.On("ReadFile", path).Return([]byte(json), nil)

	conf := parse(path, suite.ma, jsonParser{})

	suite.ma.AssertExpectations(suite.T())
	assert.Equal(suite.T(), "service", conf.FileName, "filename should be parsed properly from the path")
	assert.Equal(suite.T(), "FancyServiceName", conf.ServiceName, "Incorrect population of the config from the yaml - route.ServiceName")
	assert.Equal(suite.T(), "ServiceName", conf.Service, "Incorrect population of the config from the yaml - route.Service")
	assert.Equal(suite.T(), "testpackage", conf.Package, "Incorrect population of the config from the yaml - route.Package")
	assert.Len(suite.T(), conf.Routes, 2, "2 routes must be parsed")
	assert.True(suite.T(), assert.ObjectsAreEqualValues([]string{"GET", "POST"}, conf.Routes[0].getMethods()), "Multiple methods must be parsed and populated correctly")
	assert.Nil(suite.T(), conf.Routes[0].Method, "Multiple methods must be parsed and populated correctly (single missing)")
	assert.Equal(suite.T(), "GET", *conf.Routes[1].Method, "Single method must be parsed and populated correctly")
	assert.Nil(suite.T(), conf.Routes[1].Methods, "Single method must be parsed and populated correctly (multi missing)")
	assert.Equal(suite.T(), "/hello/{name}", conf.Routes[0].Path, "Incorrect population of the config from the yaml - route[0].path")
	assert.Equal(suite.T(), "/foo", conf.Routes[1].Path, "Incorrect population of the config from the yaml - route[1].path")
	assert.Equal(suite.T(), "SayHello", conf.Routes[0].Function, "Incorrect population of the config from the yaml - route[0].function")
	assert.Equal(suite.T(), "MakeFoo", conf.Routes[1].Function, "Incorrect population of the config from the yaml - route[1].function")
	assert.Equal(suite.T(), "FooServiceName", conf.Routes[1].Service, "Incorrect population of the config from the yaml - route[1].service")
}

func TestConfigSuit(t *testing.T) {
	suite.Run(t, new(ConfigTestSuite))
}
